﻿namespace Proyectolll.GUI
{
    partial class FrmPaises
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPaises));
            this.panel1 = new System.Windows.Forms.Panel();
            this.listPaises = new System.Windows.Forms.ListBox();
            this.tCountryCodeAndNameBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.cbxCont = new System.Windows.Forms.ComboBox();
            this.tContinentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tCountryCodeAndNameGroupedByContinentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tCountryInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnMinimizar = new System.Windows.Forms.PictureBox();
            this.btnCerrar = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tCountryCodeAndNameBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tContinentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tCountryCodeAndNameGroupedByContinentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tCountryInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.listPaises);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbxCont);
            this.panel1.Location = new System.Drawing.Point(153, 10);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(349, 424);
            this.panel1.TabIndex = 0;
            // 
            // listPaises
            // 
            this.listPaises.DataSource = this.tCountryCodeAndNameBindingSource;
            this.listPaises.DisplayMember = "sName";
            this.listPaises.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listPaises.FormattingEnabled = true;
            this.listPaises.ItemHeight = 17;
            this.listPaises.Location = new System.Drawing.Point(16, 116);
            this.listPaises.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listPaises.Name = "listPaises";
            this.listPaises.Size = new System.Drawing.Size(188, 293);
            this.listPaises.TabIndex = 4;
            this.listPaises.ValueMember = "sISOCode";
            // 
            // tCountryCodeAndNameBindingSource
            // 
            this.tCountryCodeAndNameBindingSource.DataSource = typeof(Proyectolll.org.oorsprong.webservices.tCountryCodeAndName);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Continentes";
            // 
            // cbxCont
            // 
            this.cbxCont.DataSource = this.tContinentBindingSource;
            this.cbxCont.DisplayMember = "sName";
            this.cbxCont.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxCont.FormattingEnabled = true;
            this.cbxCont.Location = new System.Drawing.Point(16, 59);
            this.cbxCont.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxCont.Name = "cbxCont";
            this.cbxCont.Size = new System.Drawing.Size(132, 25);
            this.cbxCont.TabIndex = 2;
            this.cbxCont.ValueMember = "sCode";
            this.cbxCont.SelectedIndexChanged += new System.EventHandler(this.CbxCont_SelectedIndexChanged);
            // 
            // tContinentBindingSource
            // 
            this.tContinentBindingSource.DataSource = typeof(Proyectolll.org.oorsprong.webservices.tContinent);
            // 
            // tCountryCodeAndNameGroupedByContinentBindingSource
            // 
            this.tCountryCodeAndNameGroupedByContinentBindingSource.DataSource = typeof(Proyectolll.org.oorsprong.webservices.tCountryCodeAndNameGroupedByContinent);
            // 
            // tCountryInfoBindingSource
            // 
            this.tCountryInfoBindingSource.DataSource = typeof(Proyectolll.org.oorsprong.webservices.tCountryInfo);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Proyectolll.Properties.Resources.back_to_40px;
            this.pictureBox1.Location = new System.Drawing.Point(11, 379);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(54, 55);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimizar.Image")));
            this.btnMinimizar.Location = new System.Drawing.Point(600, 10);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(15, 15);
            this.btnMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMinimizar.TabIndex = 16;
            this.btnMinimizar.TabStop = false;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Image")));
            this.btnCerrar.Location = new System.Drawing.Point(630, 10);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(15, 15);
            this.btnCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnCerrar.TabIndex = 15;
            this.btnCerrar.TabStop = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // FrmPaises
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(227)))), ((int)(((byte)(129)))));
            this.ClientSize = new System.Drawing.Size(657, 449);
            this.Controls.Add(this.btnMinimizar);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrmPaises";
            this.Text = "FrmPaises";
            this.Load += new System.EventHandler(this.FrmPaises_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmPaises_MouseDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tCountryCodeAndNameBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tContinentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tCountryCodeAndNameGroupedByContinentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tCountryInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox listPaises;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxCont;
        private System.Windows.Forms.BindingSource tContinentBindingSource;
        private System.Windows.Forms.BindingSource tCountryCodeAndNameGroupedByContinentBindingSource;
        private System.Windows.Forms.BindingSource tCountryCodeAndNameBindingSource;
        private System.Windows.Forms.BindingSource tCountryInfoBindingSource;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox btnMinimizar;
        private System.Windows.Forms.PictureBox btnCerrar;
    }
}