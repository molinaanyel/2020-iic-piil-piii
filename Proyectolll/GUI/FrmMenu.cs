﻿using Proyectolll.BOL;
using Proyectolll.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyectolll.GUI
{
    public partial class FrmMenu : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        EEstudiante est;

        public FrmMenu()
        {
            InitializeComponent();
            this.CenterToScreen();
            CargarDatos();
        }
        public FrmMenu(string cedEst)
        {
            InitializeComponent();
            this.CenterToScreen();
            est = new EstudianteBOL().Cargar(cedEst);
            CargarDatos();
        }

        private void CargarDatos()
        {
            if (est != null)
            {
                lblUsuario.Text = est.nombre + " - Estudiante";
                btnReportes.Visible = false;
            }
            else
            {
                lblUsuario.Text = "lvras - Admin";
            }
        }

        private void FrmMenu_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel13_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Logout(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void btnContinentes_Click(object sender, EventArgs e)
        {
            if (est != null)
            {
                FrmContinentes frm = new FrmContinentes(est.cedula);
                frm.Show();
                frm.FormClosed += Logout;
                this.Hide();
            }
            else
            {
                FrmContinentes frm = new FrmContinentes();
                frm.Show();
                frm.FormClosed += Logout;
                this.Hide();
            }           
        }

        private void btnPaises_Click(object sender, EventArgs e)
        {
            if (est != null)
            {
                FrmPaises frm = new FrmPaises(est.cedula);
                frm.Show();
                frm.FormClosed += Logout;
                this.Hide();
            }
            else
            {
                FrmPaises frm = new FrmPaises();
                frm.Show();
                frm.FormClosed += Logout;
                this.Hide();
            }
        }

        private void btnCapitales_Click(object sender, EventArgs e)
        {
            if (est != null)
            {
                FrmCapitales frm = new FrmCapitales(est.cedula);
                frm.Show();
                frm.FormClosed += Logout;
                this.Hide();
            }
            else
            {
                FrmCapitales frm = new FrmCapitales();
                frm.Show();
                frm.FormClosed += Logout;
                this.Hide();
            }
        }

        private void btnBanderas_Click(object sender, EventArgs e)
        {
            if (est != null)
            {
                FrmBanderas frm = new FrmBanderas(est.cedula);
                frm.Show();
                frm.FormClosed += Logout;
                this.Hide();
            }
            else
            {
                FrmBanderas frm = new FrmBanderas();
                frm.Show();
                frm.FormClosed += Logout;
                this.Hide();
            }
        }

        private void btnMonedas_Click(object sender, EventArgs e)
        {
            if (est != null)
            {
                FrmMoneda frm = new FrmMoneda(est.cedula);
                frm.Show();
                frm.FormClosed += Logout;
                this.Hide();
            }
            else
            {
                FrmMoneda frm = new FrmMoneda();
                frm.Show();
                frm.FormClosed += Logout;
                this.Hide();
            }
        }

        private void btnIdiomas_Click(object sender, EventArgs e)
        {
            if (est != null)
            {
                FrmIdiomas frm = new FrmIdiomas(est.cedula);
                frm.Show();
                frm.FormClosed += Logout;
                this.Hide();
            }
            else
            {
                FrmIdiomas frm = new FrmIdiomas();
                frm.Show();
                frm.FormClosed += Logout;
                this.Hide();
            }
        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar sesion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            FrmReportes frm = new FrmReportes();
            frm.Show();
            frm.FormClosed += Logout;
            this.Hide();
        }
    }
}
