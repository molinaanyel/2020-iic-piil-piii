﻿using Proyectolll.BOL;
using Proyectolll.DAL;
using Proyectolll.Entities;
using Proyectolll.org.oorsprong.webservices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyectolll.GUI
{
    public partial class FrmContinentes : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        EEstudiante est;
        string contBusq;

        public FrmContinentes()
        {
            InitializeComponent();
            CenterToScreen();
        }

        public FrmContinentes(string ced)
        {
            InitializeComponent();
            CenterToScreen();
            est = new EstudianteBOL().Cargar(ced);
        }

        private void FrmContinentes_Load(object sender, EventArgs e)
        {
            CountryInfoService infoService = new CountryInfoService();
            cbxCont.DataSource = infoService.ListOfContinentsByCode();
            EBusqCant bqc = new EBusqCant();
            bqc.tipo = "Continentes";
            new BusqCantDAL().InsertarDatos(bqc);
        }

        private void cbxCont_SelectedIndexChanged(object sender, EventArgs e)
        {
            CountryInfoService infoService = new CountryInfoService();
            tContinent cont = cbxCont.SelectedItem as tContinent;
            if (cont.sCode.Equals("AF"))
            {
                pcbContinente.Image = Proyectolll.Properties.Resources.africa;
                lblContiente.Text = "Africa";
                contBusq = "Africa";
            }
            if (cont.sCode.Equals("AM"))
            {
                pcbContinente.Image = Proyectolll.Properties.Resources.america__1_;
                lblContiente.Text = "America";
                contBusq = "America";
            }
            if (cont.sCode.Equals("AN"))
            {
                pcbContinente.Image = Proyectolll.Properties.Resources.hielo;
                lblContiente.Text = "Antartica";
                contBusq = "Antartica";
            }
            if (cont.sCode.Equals("AS"))
            {
                pcbContinente.Image = Proyectolll.Properties.Resources.asia;
                lblContiente.Text = "Asia";
                contBusq = "Asia";
            }
            if (cont.sCode.Equals("EU"))
            {
                pcbContinente.Image = Proyectolll.Properties.Resources.mapa;
                lblContiente.Text = "Europa";
                contBusq = "Europa";
            }
            if (cont.sCode.Equals("OC"))
            {
                pcbContinente.Image = Proyectolll.Properties.Resources.australia;
                lblContiente.Text = "Oceania";
                contBusq = "Oceania";
            }
        }

        private void FrmContinentes_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (est != null)
            {
                EBusqueda b = new EBusqueda();
                b.cedEstudiante = est.cedula;
                b.busqueda = "Continente de " + contBusq;
                new BusquedaBOL().Insertar(b);
            }
            this.Close();
        }
    }
}
