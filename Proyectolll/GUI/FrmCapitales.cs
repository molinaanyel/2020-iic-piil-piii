﻿using Proyectolll.BOL;
using Proyectolll.DAL;
using Proyectolll.Entities;
using Proyectolll.org.oorsprong.webservices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyectolll.GUI
{
    public partial class FrmCapitales : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        EEstudiante est;
        tCountryInfo paisBusq;

        public FrmCapitales()
        {
            InitializeComponent();
            CenterToScreen();
        }

        public FrmCapitales(string ced)
        {
            InitializeComponent();
            CenterToScreen();
            est = new EstudianteBOL().Cargar(ced);
        }

        private void FrmCapitales_Load(object sender, EventArgs e)
        {
            CountryInfoService infoService = new CountryInfoService();
            cbxCont.DataSource = infoService.ListOfContinentsByCode();
            lblTelefono.Visible = false;
            lblcod.Visible = false;
            EBusqCant bqc = new EBusqCant();
            bqc.tipo = "Capitales";
            new BusqCantDAL().InsertarDatos(bqc);
        }

        private void cbxCont_SelectedIndexChanged(object sender, EventArgs e)
        {
            CountryInfoService infoService = new CountryInfoService();
            tContinent cont = cbxCont.SelectedItem as tContinent;
            List<tCountryCodeAndName> paises = new List<tCountryCodeAndName>();
            foreach (tCountryCodeAndNameGroupedByContinent continente in infoService.ListOfCountryNamesGroupedByContinent())
            {

                if (continente.Continent.sCode.Equals(cont.sCode))
                {

                    foreach (tCountryCodeAndName pais in continente.CountryCodeAndNames)
                    {
                        paises.Add(pais);
                    }
                }

            }
            cbxPais.DataSource = paises;
        }

        private void cbxPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            CountryInfoService infoService = new CountryInfoService();
            tCountryCodeAndName pais = cbxPais.SelectedItem as tCountryCodeAndName;
            tCountryInfo p = new tCountryInfo();
            p = infoService.FullCountryInfo(pais.sISOCode);
            paisBusq = infoService.FullCountryInfo(pais.sISOCode);
            lblCodigo.Text = p.sISOCode;
            lblCapital.Text = p.sCapitalCity;
            lblTelefono.Text = "+ " + p.sPhoneCode;          
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (est != null)
            {
                EBusqueda b = new EBusqueda();
                b.cedEstudiante = est.cedula;
                b.busqueda = "Capital de " + paisBusq.sName;
                new BusquedaBOL().Insertar(b);
            }
            this.Close();
        }

        private void FrmCapitales_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void chkCodTel_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCodTel.Checked == true)
            {
                lblTelefono.Visible = true;
                lblcod.Visible = true;
            }
            else if (chkCodTel.Checked == false)
            {
                lblTelefono.Visible = false;
                lblcod.Visible = false;
            }
            
        }
    }
}
