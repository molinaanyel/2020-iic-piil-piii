﻿using Proyectolll.BOL;
using Proyectolll.DAL;
using Proyectolll.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyectolll.GUI
{
    public partial class FrmReportes : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        public FrmReportes()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void FrmReportes_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmReportes_Load(object sender, EventArgs e)
        {
            cbxEstudiante.DataSource = new EstudianteBOL().CargarEstudiantes();
        }

        private void cbxEstudiante_SelectedIndexChanged(object sender, EventArgs e)
        {
            EEstudiante est = cbxEstudiante.SelectedItem as EEstudiante;
            EUsuario usu = new UsuarioBOL().CargarUsu(est.cedula);
            lblUsuario.Text = usu.usuario;

            dgvReporte1.Rows.Clear();
            foreach (EBusqueda a in new BusquedaBOL().CargarBusquedas(est.cedula))
            {
                dgvReporte1.Rows.Add(a.busqueda);
            }
        }

        private void cbxTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            string tipo = cbxTipo.SelectedItem.ToString();
            dgvReporte2.Rows.Clear();
            int cant = new BusqCantDAL().ConsultarCant(tipo);
            dgvReporte2.Rows.Add(tipo, cant);
        }
    }
}
