﻿using Proyectolll.BOL;
using Proyectolll.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyectolll.GUI
{
    public partial class FrmRegistro : Form
    {

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        public FrmRegistro()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void FrmRegistro_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void Logout(object sender, FormClosedEventArgs e)
        {
            txtUsuario.ResetText();
            txtContrasenna.ResetText();
            lblMensaje.Visible = false;
            this.Show();
        }

        private void txtCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void msgError(string msg)
        {
            lblMensaje.Text = "         " + msg;
            lblMensaje.Visible = true;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUsuario.Text != "")
            {
                if (txtContrasenna.Text != "")
                {
                    EUsuario u = new EUsuario();
                    u.usuario = txtUsuario.Text.Trim();
                    u.contrasenna = txtContrasenna.Text.Trim();
                    u = new UsuarioBOL().Login(u);
                    if (u != null)
                    {
                        if (u.tipo == "Administrador")
                        {
                            FrmMenu frm = new FrmMenu();
                            frm.Show();
                            frm.FormClosed += Logout;
                            this.Hide();
                        }
                        else if (u.tipo == "Estudiante")
                        {
                            FrmMenu frm = new FrmMenu(u.cedEstudiante);
                            frm.Show();
                            frm.FormClosed += Logout;
                            this.Hide();
                        }
                    }
                    else
                    {
                        msgError("Cedula o contraseña incorrecta");
                        txtUsuario.Clear();
                        txtContrasenna.Clear();
                    }
                }
                else
                {
                    msgError("Por favor digite su contraseña");
                }
            }
            else
            {
                msgError("Por favor digite su cedula");
            }
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (txtCedula.Text != "")
            {
                if (txtNombre.Text != "")
                {
                    if (txtContra.Text != "")
                    {
                        
                        EUsuario u = new EUsuario();
                        u.usuario = txtUsu.Text.Trim();
                        u.contrasenna = txtContra.Text.Trim();
                        u.tipo = "Estudiante";
                        u.cedEstudiante = txtCedula.Text.Trim();
                        Console.WriteLine(u);
                        new UsuarioBOL().Insertar(u);

                        EEstudiante es = new EEstudiante();
                        es.nombre = txtNombre.Text.Trim();
                        es.cedula = txtCedula.Text.Trim();
                        if (rdbFemenino.Checked == true)
                        {
                            es.genero = "Femenino";
                        }
                        else if (rdbMasculino.Checked == true)
                        {
                            es.genero = "Masculino";
                        }
                        es.fechaNacimiento = dtpFechaNaci.Value;
                        Console.WriteLine(es);
                        new EstudianteBOL().Insertar(es);
                        MessageBox.Show("Estudiante Registrado", "Informacion");
                        LimpiarRegistro();


                    }
                    else
                    {
                        msgError("Por favor digite su Contraseña");
                    }
                }
                else
                {
                    msgError("Por favor digite su nombre");
                }
            }
            else
            {
                msgError("Por favor digite su cedula");
            }
        }

        private void LimpiarRegistro()
        {
            txtCedula.ResetText();
            txtContra.ResetText();
            txtNombre.ResetText();
            txtUsu.ResetText();
            dtpFechaNaci.ResetText();
            lblMensaje.Visible = false;
        }

    }
}
