﻿namespace Proyectolll.GUI
{
    partial class FrmBanderas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBanderas));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxCont = new System.Windows.Forms.ComboBox();
            this.tContinentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cbxPais = new System.Windows.Forms.ComboBox();
            this.tCountryCodeAndNameBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pbxBandera = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnMinimizar = new System.Windows.Forms.PictureBox();
            this.btnCerrar = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.tContinentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tCountryCodeAndNameBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxBandera)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(87, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Continente";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(295, 24);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "País";
            // 
            // cbxCont
            // 
            this.cbxCont.DataSource = this.tContinentBindingSource;
            this.cbxCont.DisplayMember = "sName";
            this.cbxCont.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxCont.FormattingEnabled = true;
            this.cbxCont.Location = new System.Drawing.Point(90, 76);
            this.cbxCont.Margin = new System.Windows.Forms.Padding(2);
            this.cbxCont.Name = "cbxCont";
            this.cbxCont.Size = new System.Drawing.Size(142, 25);
            this.cbxCont.TabIndex = 2;
            this.cbxCont.ValueMember = "sCode";
            this.cbxCont.SelectedIndexChanged += new System.EventHandler(this.CbxCont_SelectedIndexChanged);
            // 
            // tContinentBindingSource
            // 
            this.tContinentBindingSource.DataSource = typeof(Proyectolll.org.oorsprong.webservices.tContinent);
            // 
            // cbxPais
            // 
            this.cbxPais.DataSource = this.tCountryCodeAndNameBindingSource;
            this.cbxPais.DisplayMember = "sName";
            this.cbxPais.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxPais.FormattingEnabled = true;
            this.cbxPais.Location = new System.Drawing.Point(298, 76);
            this.cbxPais.Margin = new System.Windows.Forms.Padding(2);
            this.cbxPais.Name = "cbxPais";
            this.cbxPais.Size = new System.Drawing.Size(138, 25);
            this.cbxPais.TabIndex = 3;
            this.cbxPais.ValueMember = "sISOCode";
            this.cbxPais.SelectedIndexChanged += new System.EventHandler(this.CbxPais_SelectedIndexChanged);
            // 
            // tCountryCodeAndNameBindingSource
            // 
            this.tCountryCodeAndNameBindingSource.DataSource = typeof(Proyectolll.org.oorsprong.webservices.tCountryCodeAndName);
            // 
            // pbxBandera
            // 
            this.pbxBandera.Location = new System.Drawing.Point(90, 161);
            this.pbxBandera.Margin = new System.Windows.Forms.Padding(2);
            this.pbxBandera.Name = "pbxBandera";
            this.pbxBandera.Size = new System.Drawing.Size(345, 199);
            this.pbxBandera.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxBandera.TabIndex = 4;
            this.pbxBandera.TabStop = false;           
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(227)))), ((int)(((byte)(129)))));
            this.panel1.Controls.Add(this.pbxBandera);
            this.panel1.Controls.Add(this.cbxPais);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cbxCont);
            this.panel1.Location = new System.Drawing.Point(97, 11);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(527, 408);
            this.panel1.TabIndex = 5;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Proyectolll.Properties.Resources.back_to_40px;
            this.pictureBox1.Location = new System.Drawing.Point(21, 354);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(54, 55);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimizar.Image")));
            this.btnMinimizar.Location = new System.Drawing.Point(651, 11);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(15, 15);
            this.btnMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMinimizar.TabIndex = 16;
            this.btnMinimizar.TabStop = false;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click_1);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Image")));
            this.btnCerrar.Location = new System.Drawing.Point(681, 11);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(15, 15);
            this.btnCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnCerrar.TabIndex = 15;
            this.btnCerrar.TabStop = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click_1);
            // 
            // FrmBanderas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(227)))), ((int)(((byte)(129)))));
            this.ClientSize = new System.Drawing.Size(708, 420);
            this.Controls.Add(this.btnMinimizar);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmBanderas";
            this.Text = "FrmBanderas";
            this.Load += new System.EventHandler(this.FrmBanderas_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmBanderas_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.tContinentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tCountryCodeAndNameBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxBandera)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxCont;
        private System.Windows.Forms.ComboBox cbxPais;
        private System.Windows.Forms.PictureBox pbxBandera;
        private System.Windows.Forms.BindingSource tContinentBindingSource;
        private System.Windows.Forms.BindingSource tCountryCodeAndNameBindingSource;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox btnMinimizar;
        private System.Windows.Forms.PictureBox btnCerrar;
    }
}