﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Proyectolll.BOL;
using Proyectolll.DAL;
using Proyectolll.Entities;
using Proyectolll.org.oorsprong.webservices;

namespace Proyectolll.GUI
{
    public partial class FrmPaises : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        EEstudiante est;
        tContinent contBusq;

        public FrmPaises()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        public FrmPaises(string ced)
        {
            InitializeComponent();
            this.CenterToScreen();
            est = new EstudianteBOL().Cargar(ced);
        }

        private void FrmPaises_Load(object sender, EventArgs e)
        {
            CountryInfoService infoService = new CountryInfoService();     
            cbxCont.DataSource = infoService.ListOfContinentsByCode();
            EBusqCant bqc = new EBusqCant();
            bqc.tipo = "Paises";
            new BusqCantDAL().InsertarDatos(bqc);
        }

        private void CbxCont_SelectedIndexChanged(object sender, EventArgs e)
        {
            CountryInfoService infoService = new CountryInfoService();
            tContinent cont = cbxCont.SelectedItem as tContinent;
            contBusq = cbxCont.SelectedItem as tContinent;
            List<tCountryCodeAndName> paises = new List<tCountryCodeAndName>();
            
            foreach (tCountryCodeAndNameGroupedByContinent continente in infoService.ListOfCountryNamesGroupedByContinent())
            {              
                if (continente.Continent.sCode.Equals(cont.sCode))
                {                   
                    foreach (tCountryCodeAndName pais in continente.CountryCodeAndNames)
                    {
                        paises.Add(pais);
                        
                    }
                }
            }
            listPaises.DataSource = paises;
        }

        private void FrmPaises_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro que desea cerrar la aplicacion ?", "Alerta"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (est != null)
            {
                EBusqueda b = new EBusqueda();
                b.cedEstudiante = est.cedula;
                b.busqueda = "Paises del continente " + contBusq.sName;
                new BusquedaBOL().Insertar(b);
            }
            this.Close();
        }
    }
}


