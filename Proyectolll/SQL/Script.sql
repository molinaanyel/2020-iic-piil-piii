CREATE DATABASE progra3;

CREATE TABLE estudiantes
(
	id serial PRIMARY KEY,
	nombre text NOT NULL,
	cedula text NOT NULL,
	fecha_nacimiento date NOT NULL,
	genero text NOT NULL,
	CONSTRAINT unq_estudiantes_cedula UNIQUE (cedula)	
);

CREATE TABLE usuarios
(
	id serial PRIMARY KEY,
	usuario text NOT NULL,
	contrasenna text NOT NULL,
	tipo text NOT NULL,
	ced_estudiante text,
	CONSTRAINT unq_usuarios_usuario UNIQUE (usuario)	
);

CREATE TABLE busquedas
(
	id serial PRIMARY KEY,
	ced_estudiante text NOT NULL,
	busqueda text NOT NULL
);

CREATE TABLE cant_busq
(
	id serial PRIMARY KEY,
	tipo text NOT NULL
);

select * from cant_busq;
select * from busquedas;
select * from usuarios;
select * from estudiantes;