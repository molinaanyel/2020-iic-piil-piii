﻿using Proyectolll.DAL;
using Proyectolll.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyectolll.BOL
{
    class BusquedaBOL
    {

        /// <summary>
        /// Comprueba que la info este correcta antes de insertar
        /// </summary>
        /// <param name="b"></param>
        public void Insertar(EBusqueda b)
        {
            if (b != null)
            {
                new BusquedaDAL().InsertarDatos(b);
            }
        }

        /// <summary>
        /// Consulta una lista de busquedas segun la cedula del estudiante
        /// </summary>
        /// <param name="ced"></param> cedula
        /// <returns></returns> lista de busquedas
        public List<EBusqueda> CargarBusquedas(string ced)
        {
            return new BusquedaDAL().ConsultarDatos(ced);
        }

    }
}
