﻿using Proyectolll.DAL;
using Proyectolll.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Proyectolll.BOL
{
    class UsuarioBOL
    {
        /// <summary>
        /// Comprueba que la info del usuario va lista para consultar
        /// e incripta el password
        /// </summary>
        /// <param name="u"></param> usuario
        /// <returns></returns> usuario en caso de encontrarlo
        public EUsuario Login(EUsuario u)
        {
            if (u != null)
            {
                u.contrasenna = GetMD5(u.contrasenna);
                return new UsuarioDAL().Autenticar(u);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Debe crear un usuario");
                return null;
            }
        }

        /// <summary>
        /// Encripta el password y comprueba la info antes de insertar a la BD
        /// </summary>
        /// <param name="u"></param> usuario
        public void Insertar(EUsuario u)
        {
            if (u != null)
            {
                u.contrasenna = GetMD5(u.contrasenna);
                new UsuarioDAL().InsertarDatos(u);
            }
        }

        /// <summary>
        /// Hace una consulta de usuario si recibe una cedula
        /// </summary>
        /// <param name="ced"></param> cedula
        /// <returns></returns> usuario
        public EUsuario CargarUsu(string ced)
        {
            return new UsuarioDAL().CargarUsu(ced);
        }

        /// <summary>
        /// Encripta el password en MD5
        /// </summary>
        /// <param name="str"></param> password
        /// <returns></returns> password encriptado
        public string GetMD5(string str)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
    }
}
