﻿using Proyectolll.DAL;
using Proyectolll.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyectolll.BOL
{
    class EstudianteBOL
    {
        /// <summary>
        /// Comprueba la info antes de insertar
        /// </summary>
        /// <param name="e"></param> estudiante
        public void Insertar(EEstudiante e)
        {
            if (e != null)
            {
                new EstudianteDAL().InsertarDatos(e);
            }
        }

        /// <summary>
        /// Consulta segun la cedula recibida
        /// </summary>
        /// <param name="ced"></param> cedula
        /// <returns></returns> estudiante
        public EEstudiante Cargar(string ced)
        {
            return new EstudianteDAL().CargarEstudiante(ced);
        }

        /// <summary>
        /// Consulta una lista de estudiantes
        /// </summary>
        /// <returns></returns> lista de estudiantes
        public List<EEstudiante> CargarEstudiantes()
        {
            return new EstudianteDAL().ConsultarEstudiantes();
        }
    }
}
