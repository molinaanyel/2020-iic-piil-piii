﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyectolll.Entities
{
    public class EUsuario
    {
        public int id { get; set; }
        public string usuario { get; set; }        
        public string contrasenna { get; set; }
        public string tipo { get; set; }
        public string cedEstudiante { get; set; }

        public override string ToString()
        {
            return id + " " + usuario + " " + tipo  +" " + contrasenna  + " " + cedEstudiante; 
        }
    }
}
