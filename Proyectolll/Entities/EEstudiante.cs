﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyectolll.Entities
{
    class EEstudiante
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string cedula { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public string genero { get; set; }

        public override string ToString()
        {
            return nombre + " - " + cedula;
        }
    }
}
