﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyectolll.Entities
{
    class EBusqueda
    {
        public int id { get; set; }
        public string cedEstudiante { get; set; }
        public string busqueda { get; set; }

        public override string ToString()
        {
            return id + " " + cedEstudiante + " " + busqueda;
        }
    }
}
