﻿using Npgsql;
using Proyectolll.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyectolll.DAL
{
    class BusqCantDAL
    {
        private NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Inserta un registro de las busquedas que se realizan
        /// </summary>
        /// <param name="b"></param> cantidad de busquedas
        public void InsertarDatos(EBusqCant b)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO cant_busq (tipo) VALUES ('" + b.tipo + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Consulta la cantidad de busquedas realizadas en especifico
        /// </summary>
        /// <param name="tipo"></param> busqueda especifica
        /// <returns></returns> numero de busquedas
        public int ConsultarCant(string tipo)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT count(*) from cant_busq where tipo = @tipo ", conexion);
            cmd.Parameters.AddWithValue("@tipo", tipo);
            int cantidad = 0;
            cantidad = Convert.ToInt32(cmd.ExecuteScalar());
            conexion.Close();
            return cantidad;
        }

    }
}
