﻿using Npgsql;
using Proyectolll.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyectolll.DAL
{
    class UsuarioDAL
    {
        private NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Comprueba que los datos del usuario existan para hacer login
        /// </summary>
        /// <param name="u"></param> usuario enviado a comprobar
        /// <returns></returns> Usuario comprobado
        public EUsuario Autenticar(EUsuario u)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, usuario, contrasenna, tipo, ced_estudiante FROM usuarios WHERE usuario = '" + u.usuario + "' and contrasenna = '" + u.contrasenna + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            EUsuario usuario = new EUsuario();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    usuario = Cargar(dr);
                    conexion.Close();
                    return usuario;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Registra un usuario nuevo 
        /// </summary>
        /// <param name="u"></param> Usuario a registrar
        public void InsertarDatos(EUsuario u)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO usuarios (usuario, contrasenna, tipo, ced_estudiante) VALUES ('" + u.usuario + "', '" + u.contrasenna + "', '" + u.tipo + "', '" + u.cedEstudiante + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Carga un usuario 
        /// </summary>
        /// <param name="ced"></param> lo busca por cedula
        /// <returns></returns> usuario comprobado
        public EUsuario CargarUsu(string ced)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, usuario, contrasenna, tipo, ced_estudiante FROM usuarios WHERE ced_estudiante = '" + ced + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            EUsuario usuario = new EUsuario();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    usuario = Cargar(dr);
                    conexion.Close();
                    return usuario;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Carga los datos del usuario
        /// </summary>
        /// <param name="dr"></param> dataReader de la BD 
        /// <returns></returns> usuario
        private EUsuario Cargar(NpgsqlDataReader dr)
        {
            EUsuario u = new EUsuario()
            {
                id = dr.GetInt32(0),
                usuario = dr.GetString(1),
                contrasenna = dr.GetString(2),
                tipo = dr.GetString(3),
                cedEstudiante = dr.GetString(4)
            };
            return u;
        }

    }
}
