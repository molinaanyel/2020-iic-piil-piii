﻿using Npgsql;
using Proyectolll.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyectolll.DAL
{
    class EstudianteDAL
    {
        private NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Inserta un estudiante 
        /// </summary>
        /// <param name="e"></param> estudiante que insertara a la BD
        public void InsertarDatos(EEstudiante e)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO estudiantes (cedula, nombre, fecha_nacimiento, genero) VALUES ('" + e.cedula + "', '" + e.nombre + "', '" + e.fechaNacimiento + "', '" + e.genero +"')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Carga un estudiante por cedula
        /// </summary>
        /// <param name="ced"></param> filtro de cedula para buscar
        /// <returns></returns> estudiante comprobado
        public EEstudiante CargarEstudiante(string ced)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, nombre, cedula, fecha_nacimiento, genero FROM estudiantes WHERE cedula = '" + ced + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            EEstudiante estudiante = new EEstudiante();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    estudiante = Cargar(dr);
                    conexion.Close();
                    return estudiante;
                }
            }
            conexion.Close();
            return null;
        }

        /// <summary>
        /// Carga los estudiantes registrados
        /// </summary>
        /// <returns></returns> lista de estudiantes
        public List<EEstudiante> ConsultarEstudiantes()
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<EEstudiante> lista = new List<EEstudiante>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, nombre, cedula, fecha_nacimiento, genero FROM estudiantes ", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Carga la informacion de los estudiantes
        /// </summary>
        /// <param name="dr"></param> dataReader de la BD
        /// <returns></returns> estudiante
        private EEstudiante Cargar(NpgsqlDataReader dr)
        {
            EEstudiante e = new EEstudiante()
            {
                id = dr.GetInt32(0),
                nombre = dr.GetString(1),
                cedula = dr.GetString(2),
                fechaNacimiento = dr.GetDateTime(3),
                genero = dr.GetString(4)
            };
            return e;
        }

    }
}
