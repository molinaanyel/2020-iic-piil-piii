﻿using Npgsql;
using Proyectolll.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyectolll.DAL
{
    class BusquedaDAL
    {
        private NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        /// <summary>
        /// Inserta una busqueda realizada por un estudiante
        /// </summary>
        /// <param name="b"></param> busqueda 
        public void InsertarDatos(EBusqueda b)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO busquedas (ced_estudiante, busqueda) VALUES ('" + b.cedEstudiante + "', '" + b.busqueda + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Consulta las busquedas de un estudiante
        /// </summary>
        /// <param name="ced"></param> cedula del estudiante
        /// <returns></returns> lista de busquedas
        public List<EBusqueda> ConsultarDatos(string ced)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            List<EBusqueda> lista = new List<EBusqueda>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, ced_estudiante, busqueda FROM busquedas where ced_estudiante = @ced", conexion);
            cmd.Parameters.AddWithValue("@ced", ced);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            conexion.Close();
            return lista;
        }

        /// <summary>
        /// Carga la informacion de las busquedas
        /// </summary>
        /// <param name="dr"></param> dataReader de la BD
        /// <returns></returns> busqueda
        private EBusqueda Cargar(NpgsqlDataReader dr)
        {
            EBusqueda b = new EBusqueda()
            {
                id = dr.GetInt32(0),
                cedEstudiante = dr.GetString(1),
                busqueda = dr.GetString(2)

            };
            return b;
        }

    }
}
